var express = require('express');
var app = express();

app.get('/', function (req, res) {
   
    //var sql = require("mssql");
    const sql = require('mssql/msnodesqlv8');

    // config for your database
    var config = {
        user: 'usuario',
        password: 'user123',
        server: 'SISTEMAS3\\SQLEXPRESS', 
        port: 1433,
        database: 'db_chat' 
    };

    // connect to your database
    sql.connect(config, function (err) {
    
        if (err) console.log(err);

        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('select * from comentarios', function (err, recordset) {
            
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);
            
        });
    });
});

var server = app.listen(5001, function () {
    console.log('Server is running..');
});