var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var path = require('path');
var port = process.env.PORT || 3000;


app.get('/', function(req, res){
  var somePath = __dirname;
  var npat ='/index.html';
  var resolvedPath = path.resolve(somePath+npat);

    res.sendFile(resolvedPath);
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg, user){
        io.emit('chat message', user+':'+msg );
        console.log(user);
      });
    socket.on('disconnect', function(){
      console.log('user disconnected'+socket.id);
    });
    
    socket.on('chat any', function(msg,id){
        io.to(id).emit('message', msg);
      });

  });
  
  http.listen(3000, function(){
    console.log('listening on *:3000');
  });
  